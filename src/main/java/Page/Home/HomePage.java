package Page.Home;

import Page.BasePage;

public class HomePage extends BasePage {
    private final String baseUrl = "https://demoqa.com/";
    public String getBaseUrl(){
        return baseUrl;
    }
}
